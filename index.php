<?php
require ('animal.php');
require ('ape.php');
require ('frog.php');

$sheep = new animal("shaun");
echo $sheep ->name ."<br>"; //"sahun"
echo $sheep ->legs ."<br>"; // 2
echo $sheep->cold_blooded; // false

echo "<br><br>";
$sungokong = new ape("kera sakti");
echo $sungokong->name . "<br>";
echo $sungokong->legs . "<br>";
echo $sungokong->yell(); // "Auooo"

echo "<br><br>";
$kodok = new Frog("buduk");
echo $kodok->name . "<br>";
echo $kodok->legs . "<br>";
echo $kodok->yell(); // "hop-hop"
?>